# Shelf Cow Bot
## Minecraft Discord Overviewer Linker

Formats minecraft coordinates nicely, and provides a link to the overviewer map.

### Run Locally with Node
``` powershell
node .
```

### Docker

#### Build

``` powershell
docker build --tag ductoman16/minecraft-discord-overviewer-linker .
```

#### Push

``` powershell
docker push ductoman16/minecraft-discord-overviewer-linker
```

#### Run

``` powershell
docker run -d --restart unless-stopped ductoman16/minecraft-discord-overviewer-linker
```