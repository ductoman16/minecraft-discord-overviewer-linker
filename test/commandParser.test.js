import CommandParser from "../src/command-parser.js";

const commandParser = new CommandParser();

test('parses main command', () => {
    const parsed = commandParser.parseCommand('!coords add "My Cool Location Name"');
    expect(parsed.prefix).toBe("!coords");
})

test('parses sub command', () => {
    const parsed = commandParser.parseCommand('!coords add "My Cool Location Name"');
    expect(parsed.subCommand).toBe("add");
})

test('parses quoted args', () => {
    const parsed = commandParser.parseCommand('!coords add "My Cool Location Name" 12 60 230');
    expect(parsed.args[0]).toBe('"My Cool Location Name"');
})

test('parses regular args', () => {
    const parsed = commandParser.parseCommand('!coords add 12 60');
    expect(parsed.args[0]).toBe("12");
    expect(parsed.args[1]).toBe("60");
})