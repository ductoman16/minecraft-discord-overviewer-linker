import CommandParser from "./command-parser.js";
import Discord from 'discord.js';
import fs from 'fs';

const shelfCowId = "763115952228663317";

const commandParser = new CommandParser();
const client = new Discord.Client();

login(client);

client.once('ready', () => {
    console.log("coordinates linker online")
});

const dimensionMap = {
    "overworld": {
        url: "survival%20-%20overworld/survivalday",
        color: "#0099ff"
    },
    "nether": {
        url: "survival%20-%20nether/survivalnether",
        color: "#eb4034"
    },
    "end": {
        url: "survival%20-%20end/survivalend",
        color: "#a940ff"
    }
}

function login(client) {
    fs.readFile('token', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }

        client.login(data);
        console.log("logged in")
    });
}

function getCoordinates(command) {
    var args = command.args;
    if (args.length < 4) {
        return null;
    }

    const rawName = args[0];
    var name = toTitleCase(rawName.replace(/"/gi, "").trim());

    if (args.length > 4) {
        var dimension = parseDimension(args[4]);
    }
    else {
        dimension = "overworld";
    }

    return {
        name: name,
        x: args[1].trim(),
        y: args[2].trim(),
        z: args[3].trim(),
        dimension: dimension
    }
}

function parseDimension(dimension) {
    var lower = dimension.toLowerCase().trim();
    switch (lower) {
        case "nether":
        case "n":
            return "nether";

        case "end":
        case "the end":
        case "e":
            return "end";

        case "overworld":
        case "over world":
        case "o":
        default:
            return "overworld";
    }
}

function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

const commandPrefix = "!coords";

client.on("message", message => {
    if (message.author.bot) {
        return;
    }

    if (inMonitoredChannel(message)) {

        const command = commandParser.parseCommand(message.content);
        console.log(command)

        if (command.prefix !== commandPrefix) {
            return;
        }

        // console.log(message)

        switch (command.subCommand) {
            case "add":
                console.log("add coordinates");
                addCoordinates(command, message);
                break;
            case "list":
                console.log("list coordinates");
                listCoordinates(command, message);
                break;
            case "search":
                console.log("search coordinates");
                searchCoordinates(command, message);
                break;
            default:
                sendHelp(command, message);
        }
    }
});

function addCoordinates(command, message) {
    var coordinates = getCoordinates(command);
    if (!coordinates) {
        sendHelp(command, message);
        return;
    }

    sendCoordinatesMessageEmbed(coordinates, message);

    message.delete();
}

function listCoordinates(command, message) {
    var allMessages = [];

    const messageManager = message.channel.messages;

    let loadingMessage;
    message.channel.send("Loading... Please be patient, I'm just a cow").then(msg => {
        loadingMessage = msg;
    });

    getOlderMessages(messageManager, allMessages).then(() => {
        console.log("done getting messages");
        const myMessages = allMessages.filter(msg => msg.author.id === shelfCowId);
        console.log("Found " + myMessages.length + " shelf cow messages");

        const coordinateMessages = myMessages.filter(msg => isCoordinateMessage(msg))
        console.log("Found " + coordinateMessages.length + " coordinates messages");

        sendCoordinatesListMessageEmbed(coordinateMessages, message);

        if (loadingMessage) {
            loadingMessage.delete();
        }
    })
}

function isCoordinateMessage(msg) {
    return msg.embeds.length == 1 && msg.embeds[0].url && msg.embeds[0].url.includes("toyswithrules");
}

function getOlderMessages(messageManager, allMessagesList, beforeId) {
    return messageManager.fetch({ limit: 100, before: beforeId }).then(messages => {
        var messageArray = Array.from(messages.sort((msg1, msg2) => msg1.createdAt - msg2.createdAt).values());
        if (messageArray.length) {
            console.log("Got " + messageArray.length + " messages ");

            messageArray.forEach(m => allMessagesList.push(m));
            const oldestMessage = messageArray[0];

            return getOlderMessages(messageManager, allMessagesList, oldestMessage.id);
        }
    }).catch(e => console.error(e));
}

function searchCoordinates(command, message) {
    console.log("search")
}

function sendHelp(command, message) {
    switch (command.subCommand) {
        case "add":
            message.channel.send('Syntax: `!coords add "Your location name" X Y Z optional_dimension`');
            break;
        case "list":
            message.channel.send('Syntax: `!coords list optional_dimension`');
            break;
        case "search":
            message.channel.send('Syntax: `!coords search "name of location to search for"`');
            break;
        default:
            message.channel.send('Available commands: `!coords add/list/search` (`-help` for more help)');
            break;
    }
}

function sendCoordinatesMessageEmbed(coordinates, message) {
    const embed = new Discord.MessageEmbed()
        .setColor(dimensionMap[coordinates.dimension].color)
        .setTitle(coordinates.name)
        .setURL(`http://maps.toyswithrules.com.s3.amazonaws.com/world-map/index.html#/${coordinates.x}/${coordinates.y}/${coordinates.z}/-3/${dimensionMap[coordinates.dimension].url}`)
        .setDescription(`${coordinates.x}, ${coordinates.y}, ${coordinates.z} in The ` + toTitleCase(coordinates.dimension))
        .setTimestamp()
        .setFooter(message.author.username, message.author.avatarURL());

    message.channel.send(embed);
}

function sendCoordinatesListMessageEmbed(coordinatesMessageList, message) {

    var embeds = []
    Object.keys(dimensionMap).forEach(dimensionName => {
        var dimensionCoordinates = coordinatesMessageList.filter(msg => msg.embeds[0].description.toLowerCase().includes(dimensionName));
        var sorted = dimensionCoordinates.sort((a, b) => a.createdTimestamp - b.createdTimestamp)
        const dimensionEmbeds = createEmbedDescriptions(sorted).map(d => { return { description: d, dimension: dimensionName }; });
        embeds = embeds.concat(dimensionEmbeds);
    })

    embeds.forEach((embedConfig, index) => {
        const embed = new Discord.MessageEmbed()
            .setColor(dimensionMap[embedConfig.dimension].color)
            .setTitle("The " + toTitleCase(embedConfig.dimension))
            .setDescription(embedConfig.description);

        if (index == 0) {
            embed.setAuthor("All Saved Coordinates");
        }
        // if (index == embeds.length - 1) {
        //     embed.setTimestamp();
        // }

        message.channel.send(embed);
    });
}

function createEmbedDescriptions(coordinatesMessageList) {
    var descriptions = [""];
    coordinatesMessageList.forEach(msg => {
        const lastDescription = descriptions[descriptions.length - 1];

        const embed = msg.embeds[0];
        const newLink = `[${embed.title}](${msg.url})\r\n`;

        if (lastDescription.length + newLink.length > 2048) {
            descriptions.push(newLink);
        }
        else {
            descriptions[descriptions.length - 1] = lastDescription + newLink;
        }
    });
    return descriptions;
}

function inMonitoredChannel(message) {
    return message.channel.id === "752353376980697150" //bot-spam 
        || message.channel.id == "762852701272801290" //mc-coordinates;
}
