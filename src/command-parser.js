export default class CommandParser {
    parseCommand(commandText) {
        if (!commandText) {
            return {
                prefix: null,
                subCommand: null,
                args: []
            };
        }

        var split = commandText.match(/(".*?"|[^"\s]+)+(?=\s*|\s*$)/g);

        return {
            prefix: split[0],
            subCommand: split[1],
            args: split.slice(2)
        };
    }
}